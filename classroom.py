import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from datetime import datetime, timedelta
from discord_webhook import DiscordWebhook
import dateutil.parser
import sqlalchemy as db
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import time
import logging

Base = declarative_base()

_logger = logging.getLogger(__name__)

# Add you discord webhook url here
WEBHOOK_URL = ""

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/classroom.courses.readonly',
          'https://www.googleapis.com/auth/classroom.coursework.me']


class Classroom(Base):
    __tablename__ = 'classroom'
    id = db.Column("id", db.Integer(), primary_key=True)
    webhook_url = db.Column("webhook_url", db.String())
    courses = relationship("Course", backref="classroom")

    def connect(self):
        """Shows basic usage of the Classroom API.
        Prints the names of the first 10 courses the user has access to.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        return build('classroom', 'v1', credentials=creds)

    def update_courses(self):
        courses_for_sql = []
        c = self.connect().courses().list(pageSize=10).execute()
        courses = c.get('courses', [])
        courses_google_ids = [course.google_id for course in self.courses]
        if not courses:
            _logger.info("No Courses found")
        for course in courses:
            if course.get("id") not in courses_google_ids:
                vals = {
                    "google_id": course.get("id"),
                    "name": course.get("name"),
                    "classroom": self,
                    "link": course.get("alternateLink"),
                }
                new_course = Course(vals)
                courses_for_sql.append(new_course)
            else:
                for existing_course in self.courses:
                    if existing_course.google_id == course.get("id"):
                        updated_course = existing_course.update_from_google(course)
                        if updated_course:
                            courses_for_sql.append(updated_course)
                        break
        return courses_for_sql

    def update_course_work(self):
        cw_for_sql = []
        for course in self.courses:
            cw_google_ids = [cw.google_id for cw in course.course_works]
            course_works = self.connect().courses().courseWork().list(courseId=course.google_id).execute()
            for cw in course_works.get("courseWork", []):
                if cw.get("id") not in cw_google_ids:
                    vals = {
                        "name": cw.get("title"),
                        "google_id": cw.get("id"),
                        "link": cw.get("alternateLink"),
                        "course": course,
                        "created": dateutil.parser.parse(cw.get("creationTime")),
                    }
                    update_time = cw.get("update_time") and dateutil.parser.parse(cw.get("update_time"))
                    if update_time:
                        vals["update_date"] = update_time
                    due_date = self.get_due_date(cw.get("dueDate"), cw.get("dueTime"))
                    if due_date:
                        vals["due_date"] = due_date
                    new_cw = CourseWork(vals)
                    cw_for_sql.append(new_cw)
                else:
                    for existing_cw in course.course_works:
                        if existing_cw.google_id == cw.get("id"):
                            updated_cw = existing_cw.update_from_google(cw)
                            if updated_cw:
                                cw_for_sql.append(updated_cw)
                            break
        return cw_for_sql

    @staticmethod
    def get_due_date(due_date, due_time):
        if not due_date:
            return False
        if due_date and due_time.get("hours") and due_time.get("minutes"):
            datetimeString = "{}-{}-{}T{}:{}".format(
                due_date.get("year"),
                due_date.get("month"),
                due_date.get("day"),
                due_time.get("hours"),
                due_time.get("minutes")
            )
            return datetime.strptime(datetimeString, "%Y-%m-%dT%H:%M")

        datetimeString = "{}-{}-{}".format(
            due_date.get("year"),
            due_date.get("month"),
            due_date.get("day"),
        )
        return datetime.strptime(datetimeString, "%Y-%m-%d")

    def send_notification(self, content):
        if content:
            _logger.info("!!!!!!!!!!!!!!!!!!!Send Message!!!!!!!!!!!!!!!!!!")
            webhook = DiscordWebhook(url=self.webhook_url, content=content)
            webhook.execute()


class Course(Base):
    __tablename__ = 'course'
    id = db.Column("id", db.Integer(), primary_key=True)
    google_id = db.Column("google_id", db.String)
    name = db.Column("name", db.String)
    link = db.Column("link", db.String)

    classroom_id = db.Column(db.Integer, db.ForeignKey('classroom.id'))
    course_works = relationship("CourseWork", backref="course")

    def __init__(self, vals=None):
        if vals == None:
            vals = {}
        for key, value in vals.items():
            setattr(self, key, value)
        content = "**New Course!**\n" \
                  "* {}\n" \
                  "* {}".format(self.name, self.link)
        self.classroom.send_notification(content)


    def update_from_google(self, course):
        updated = False
        if self.name != course.get("name"):
            self.name = course.get("name")
            updated = True
        if self.link != course.get("alternateLink"):
            self.link = course.get("alternateLink")
            updated = True
        return updated and self


class CourseWork(Base):
    __tablename__ = 'course_work'
    id = db.Column("id", db.Integer(), primary_key=True)
    name = db.Column("name", db.String)
    google_id = db.Column("google_id", db.String)
    due_date = db.Column("due_date", db.DateTime)
    update_date = db.Column("update_date", db.DateTime)
    link = db.Column("link", db.String)
    due_in_one_day = db.Column("due_in_one_day", db.Boolean)
    due_today = db.Column("due_today", db.Boolean)

    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))

    def __init__(self, vals=None):
        if vals == None:
            vals = {}
        for key, value in vals.items():
            setattr(self, key, value)
        content = "**New Course Work!**\n" \
                  "* {}\n" \
                  "* {}".format(self.name, self.link)
        if self.due_date:
            content = "{}\n" \
                      "* Due: {}".format(content, self.due_date.strftime("%H:%M; %d.%m.%Y"))
        self.course.classroom.send_notification(content)

    def update_from_google(self, courseWork):
        updated = False
        update_time = courseWork.get("update_time")
        update_time = update_time and dateutil.parser.parse(update_time)
        due_date = Classroom.get_due_date(courseWork.get("dueDate"), courseWork.get("dueTime"))
        if self.name != courseWork.get("title"):
            self.name = courseWork.get("title")
            updated = True
        if self.update_date != update_time:
            self.update_date = update_time
            updated = True
        if self.link != courseWork.get("alternateLink"):
            self.link = courseWork.get("alternateLink")
        if self.due_date != due_date:
            self.due_date = due_date
            updated = True
        if updated:
            content = "**Course Work Updated**\n" \
                      "* {}\n" \
                      "* {}\n" \
                      "* Due: {}".format(self.name, self.link, self.due_date.strftime("%H:%M; %d.%m.%Y"))
            self.course.classroom.send_notification(content)
        return updated and self

    def send_notifications(self):
        now = datetime.now()
        content = ""
        if self.due_date.date() == now.date() and not self.due_today:
            self.due_today = True
            content = "**{}**\n" \
                      "* {}\n" \
                      "* {}\n" \
                      "* **Due: Today**\n" \
                      "* Due Date: {}"
        if self.due_date.date() == (now + timedelta(days=1)).date() and not self.due_in_one_day:
            self.due_in_one_day = True
            content = "**{}**\n" \
                      "* {}\n" \
                      "* {}\n" \
                      "* **Due: Tomorrow**\n" \
                      "* Due Date: {}"
        if content:
            content = content.format(self.course.name, self.name, self.link, self.due_date.strftime("%H:%M; %d.%m.%Y"))
            self.course.classroom.send_notification(content)
            return [self]
        return False

ENGINE = db.create_engine('postgresql://postgres:postgres@db:5432/postgres', echo=True)
Base.metadata.create_all(bind=ENGINE)
SESSION = sessionmaker(bind=ENGINE)
session = SESSION()
if not session.query(Classroom).count():
    cr = Classroom()
    cr.webhook_url = WEBHOOK_URL
    session.add(cr)
    session.commit()
session.close()

while True:
    session = SESSION()
    classrooms = session.query(Classroom)
    course_works = session.query(CourseWork)
    courses_list = []
    cw_list = []
    for classroom in classrooms:
        courses_list += classroom.update_courses()
        cw_list += classroom.update_course_work()
    for course_work in course_works:
        send = course_work.send_notifications()
        if send:
            cw_list += send
    session.bulk_save_objects(courses_list)
    session.bulk_save_objects(cw_list)
    session.commit()
    session.close()
    time.sleep((60.0 * 15) - (time.time() % (60.0 * 15)))
